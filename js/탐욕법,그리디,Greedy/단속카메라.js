function solution(routes) {
    routes.sort((l, r) => l[1] - r[1]);

    var count = 0;
    while(routes.length > 0) {
        count++;

        var base = routes.shift();
        for(var i = 0; i < routes.length; i++) {
            if(base[1] >= routes[i][0]) {
                routes.splice(i, 1);
                i--;
            }
        }
    }
    return count;
}

console.log(solution([[-20,15], [-14,-5], [-18,-13], [-5,-3]]));