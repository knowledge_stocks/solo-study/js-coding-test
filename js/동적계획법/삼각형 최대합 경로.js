function solution(triangle) {
    triangle.reverse();
    return triangle.slice(1).reduce(function(prev, values) {
        return values.map(function(val, idx) {
            return Math.max(prev[idx], prev[idx+1]) + val;
        });
    }, triangle[0])[0];
}

console.log(solution([[7], [3, 8], [8, 1, 0], [2, 7, 4, 4], [4, 5, 2, 6, 5]]));