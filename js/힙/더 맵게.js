function solution(scoville, K) {
    scoville.sort((l, r) => l- r);

    var count = 0;
    while(scoville[0] < K) {
        if(scoville.length == 1) {
            return -1;
        }
        count++;

        var a = scoville.shift();
        var b = scoville.shift();
        var newScov = a + b * 2;
        for(var i = 0; i< scoville.length; i++) {
            if(newScov <= scoville[i] || i == (scoville.length - 1)) {
                scoville.splice(i,0,newScov);
                break;
            }
        }
    }
    return count;
}

console.log(solution([1, 2, 3, 9, 10, 12], 7));