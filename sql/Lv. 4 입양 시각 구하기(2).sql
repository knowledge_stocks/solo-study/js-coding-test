-- 보호소에서는 몇 시에 입양이 가장 활발하게 일어나는지 알아보려 합니다.
-- 0시부터 23시까지, 각 시간대별로 입양이 몇 건이나 발생했는지 조회하는 SQL문을 작성해주세요.
-- 이때 결과는 시간대 순으로 정렬해야 합니다.

WITH RECURSIVE HOURS AS (
    SELECT 0 AS HOUR

    UNION ALL

    SELECT HOUR + 1
    FROM HOURS
    WHERE HOUR < 23
)

SELECT
    HOURS.HOUR,
    IFNULL(COUNTS.COUNT,0)
FROM (
    SELECT
        HOUR(DATETIME) AS `HOUR`,
        COUNT(HOUR(DATETIME)) AS `COUNT`
    FROM ANIMAL_OUTS
    GROUP BY `HOUR`
) AS COUNTS
RIGHT JOIN HOURS
    ON COUNTS.HOUR = HOURS.HOUR
GROUP BY HOURS.HOUR
ORDER BY HOURS.HOUR;

# SELECT
#     HOURS.HOUR,
#     COUNT(HOUR(OUTS.DATETIME)) AS `COUNT`
# FROM ANIMAL_OUTS AS OUTS
# RIGHT JOIN HOURS
#     ON HOUR(OUTS.DATETIME) = HOURS.HOUR
# GROUP BY HOURS.HOUR
# ORDER BY HOURS.HOUR;
