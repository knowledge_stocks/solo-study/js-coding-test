function solution(tickets) {
    return func1(tickets);
}

function func1(tickets) {
    var route = ['ICN'];

    tickets.sort(function(l, r) {
        return l[1].localeCompare(r[1]);
    });

    return (func_recur(route, tickets));
}

function func_recur(route, tickets) {
    if(tickets.length === 0) {
        return route;
    }

    var here = route[route.length - 1];
    for(var i = 0; i < tickets.length; i++) {
        if(tickets[i][0] === here) {
            var newTickets = tickets.slice();
            var newRoute = route.slice();
            newRoute.push(tickets[i][1]);
            newTickets.splice(i, 1);
            var result = func_recur(newRoute, newTickets);
            if(result) {
                return result;
            }
        }
    }

    return undefined;
}


function solution2(tickets) {
    var route = ['ICN'];
    tickets.sort((l,r) => l[1].localeCompare(r[1]));

    return bangmoon(route, tickets);

    function bangmoon(route, tickets) {
        if(tickets.length === 0) {
            return route;
        }

        var here = route[route.length - 1];
        for(var i = 0; i < tickets.length; i++) {
            if(tickets[i][0] === here) {
                var newTickets = tickets.slice();
                var newRoute = route.slice();
                newRoute.push(tickets[i][1]);
                newTickets.splice(i, 1);
                var r = bangmoon(newRoute, newTickets);
                if(r) {
                    return r;
                }
            }
        }

        return undefined;
    }
}

console.log(solution([["ICN", "JFK"], ["HND", "IAD"], ["JFK", "HND"]]));