// function solution(unclassified_clothes) {
//     var classified_clothes = {};

//     unclassified_clothes.forEach(element => {
//         if(!classified_clothes[element[1]])
//         {
//             classified_clothes[element[1]] = [];
//         }
//         classified_clothes[element[1]].push(element[0]);
//     });

//     // var combo = {};

//     // Object.keys(classified_clothes).forEach(function(_class) {
//     //     classified_clothes[_class].forEach(function(clothes) {
//     //         combo[clothes] = null;
//     //         this.forEach(combination => {
//     //             combo[combination + ',' + clothes] = null;
//     //         });
//     //     }, Object.keys(combo)); // 화살표식(람다식)으로 콜백을 전달하면 thisArg가 무시됨
//     // });

//     // return Object.keys(combo).length;

//     var result = 1;
//     Object.keys(classified_clothes).forEach(function(_class) {
//         result *= classified_clothes[_class].length + 1;
//     });

//     return result - 1;
// }

function solution(clothes) {
    var counts = clothes.reduce(function(accum, current) {
        accum[current[1]] = (accum[current[1]] ? accum[current[1]] : 0) + 1;
        return accum;
    }, {});

    var result = Object.values(counts).reduce(function(accum, current) {
        return accum *= (current + 1);
    }, 1) - 1;

    return result;
}

console.log(solution([["yellowhat", "headgear"], ["bluesunglasses", "eyewear"], ["green_turban", "headgear"]]));