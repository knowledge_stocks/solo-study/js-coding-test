function solution(brown, yellow) {
    // brown == ((w - 1) + (h - 1)) * 2
    // yellow == (w - 2) * (h - 2)
    // w > 2, h > 2

    for(var h = 3; h <= brown / 2 - 1; h++) {
        var w = brown / 2 - h + 2;
        if((w - 2) * (h - 2) === yellow) {
            return [w, h];
        }
    }
    return undefined;
}

console.log(solution(10,2));
console.log(solution(8, 1));
console.log(solution(24,24));