function solution(arr) {
    var record = new Map();

    arr.forEach(v => {
        if(record.has(v)) {
            record.set(v, record.get(v) + 1);
        } else {
            record.set(v, 1);
        }
    });

    var answer = [...record.values()].filter(v => v > 1);
    return answer.length > 0 ? answer : [-1];
}

console.log(solution([1, 2, 3, 3, 3, 3, 4, 4]));
console.log(solution( [3, 2, 5, 4, 4, 4, 5, 5, 5, 2, 4, 2, 4, 4]));
console.log(solution([3, 5, 7, 9, 1]));