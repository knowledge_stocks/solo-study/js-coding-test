function solution(priorities, location) {
    var answer = 0;

    while(priorities.length > 0) {
        var current = priorities.shift();
        if(current >= Math.max(...priorities)) {
            answer++;
            if(!location) {
                return answer;
            }
        } else {
            priorities.push(current);
        }
        location = location ? location - 1 : priorities.length - 1;
    }
    return answer;
}

console.log(solution([2, 1, 3, 2], 2));