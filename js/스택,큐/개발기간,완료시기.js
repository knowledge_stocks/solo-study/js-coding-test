function solution(progresses, speeds) {
    // var answer = [];

    // while(progresses.length > 0) {
    //     var needDays = Math.ceil((100 - progresses.shift()) / speeds.shift());
    //     var count = 1;
    //     while(((100 - progresses[0]) / speeds[0]) <= needDays){
    //         count++;
    //         progresses.shift();
    //         speeds.shift();
    //     }
    //     answer.push(count);
    // }

    // return answer;

    return progresses.reduce(function(accum, current, index) {
        var need_days = Math.ceil((100 - current) / speeds[index]);
        if(need_days <= accum.days){
            accum.result[accum.result.length-1]++;
        } else {
            accum.result.push(1);
            accum.days = need_days;
        }
        return accum;
    }, {days: 0, result: []}).result;
}

console.log(solution([93, 30, 55], [1, 30, 5]));