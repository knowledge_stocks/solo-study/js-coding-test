function quickSort(arr) {
    return quickSortRecur(0, arr.length - 1, arr);
    function quickSortRecur(left, right, arr) {
        if(left >= right) {
            return;
        }

        let [origin_left, origin_right] = [left, right];
        let pivot = arr[Math.floor((left + right)/2)];

        while(left <= right) {
            while(arr[left] < pivot) {
                left++;
            }
            while(arr[right] > pivot) {
                right--;
            }

            if(left <= right) {
                [arr[left], arr[right]] = [arr[right], arr[left]];
                left++;
                right--;
            }
        }

        // console.log(origin_left, origin_right, left, arr);

        quickSortRecur(origin_left, left - 1, arr);
        quickSortRecur(left, origin_right, arr);

        return arr;
    }
}

function quickSortCustom(arr, comparer) {
    if(!comparer) {
        comparer = (l, r) => l-r;
    }

    return quickSortRecur(0, arr.length - 1, arr);
    function quickSortRecur(left, right, arr) {
        if(left >= right) {
            return;
        }

        let [origin_left, origin_right] = [left, right];
        let pivot = arr[Math.floor((left + right)/2)];

        while(left <= right) {
            while(comparer(arr[left], pivot) < 0) {
                left++;
            }
            while(comparer(arr[right], pivot) > 0) {
                right--;
            }

            if(left <= right) {
                [arr[left], arr[right]] = [arr[right], arr[left]];
                left++;
                right--;
            }
        }

        // console.log(origin_left, origin_right, left, arr);

        quickSortRecur(origin_left, left - 1, arr);
        quickSortRecur(left, origin_right, arr);

        return arr;
    }
}

console.log(quickSort([6,3,8,5,2]));
console.log(quickSort([5, 8, 1, 3, 9, 4, 7]));
console.log(quickSort([1, 2, 3, 4, 5, 6, 7]));

console.log(quickSortCustom([6,3,8,5,2]));
console.log(quickSortCustom([5, 8, 1, 3, 9, 4, 7]));
console.log(quickSortCustom([1, 2, 3, 4, 5, 6, 7]));

console.log(quickSortCustom([6,3,8,5,2], (l ,r) => r-l));
console.log(quickSortCustom([5, 8, 1, 3, 9, 4, 7], (l ,r) => r-l));
console.log(quickSortCustom([1, 2, 3, 4, 5, 6, 7], (l ,r) => r-l));