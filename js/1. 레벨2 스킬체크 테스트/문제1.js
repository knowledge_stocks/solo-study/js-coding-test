function solution(dirs) {
    var lines = new Set();

    var x = 5;
    var y = 5;
    for(var command of dirs) {
        switch(command) {
            case 'U' :
                var newY = y === 10 ? y : y + 1;
                if(newY !== y) {
                    lines.add(x + ',' + y + '/' + newY);
                }
                y = newY;
                break;
            case 'D' :
                var newY = y === 0 ? y : y - 1;
                if(newY !== y) {
                    lines.add(x + ',' + newY + '/' + y);
                }
                y = newY;
                break;
            case 'R' :
                var newX = x === 10 ? x : x + 1;
                if(newX !== x) {
                    lines.add(x + '/' + newX + ',' + y);
                }
                x = newX;
                break;
            case 'L' :
                var newX = x === 0 ? x : x - 1;
                if(newX !== x) {
                    lines.add(newX + '/' + x + ',' + y);
                }
                x = newX;
                break;
        }
    }
    return lines.size;
}

console.log(solution("ULURRDLLU",7));
console.log(solution("LULLLLLLU",7));