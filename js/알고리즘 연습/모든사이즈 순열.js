// 모든 크기의 순열을 찾아주는 함수
function allSizePermutation(arr) {
    return recur([], arr);

    function recur(selected, arr) {
        if (arr.length === 0) {
            return [];
        }

        let result = [];
        arr.forEach((v, idx) => {
            let currentPermutation = [...selected, v];
            result.push(currentPermutation);
            let subPermutations = recur(currentPermutation, [...arr.slice(0, idx), ...arr.slice(idx + 1)]);
            result.push(...subPermutations);
        });

        return result;
    }
}

console.log(allSizePermutation([1,2,3,4]));
console.log(allSizePermutation([0, 3, 7]));