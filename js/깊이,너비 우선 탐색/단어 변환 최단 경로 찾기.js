function solution(begin, target, words) {
    var result = func(0, begin, target, words);
    return result !== undefined ? result : 0;
}

function func(count, current, target, words) {
    if(current === target) {
        return count;
    }

    var min_count = undefined;
    for(var i = 0; i < words.length; i++) {
        var diff_count = 0;
        for(var j = 0; j < current.length; j++) {
            if(words[i][j] !== current[j]) {
                if(diff_count++)
                {
                    break;
                }
            }
        }
        if(diff_count === 1) {
            var new_words = words.slice();
            new_words.splice(i, 1);
            var r = func(count + 1, words[i], target, new_words);
            if(r) {
                min_count = min_count !== undefined ? Math.min(min_count, r) : r;
            }
        }
    }

    return min_count;
}

console.log(solution("hit","cog",["hot", "dot", "dog", "lot", "log", "cog"]));