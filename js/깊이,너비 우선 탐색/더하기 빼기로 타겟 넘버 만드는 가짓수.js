function solution(numbers, target) {
    return func(0, numbers, target);
}

function func(val, numbers, target) {
    if(numbers.length === 0) {
        return (val === target) * 1;
    }
    return func(val + numbers[0], numbers.slice(1), target)
        + func(val - numbers[0], numbers.slice(1), target);
}

console.log(solution([1, 1, 1, 1, 1], 3));