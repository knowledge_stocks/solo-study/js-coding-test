// function solution(jobs) {
//     var heapq = require('heap-js');

//     var time = 0;
//     var heap = new heapq.Heap(function(l, r) {
//         if(l[0] <= time && r[0] <= time) {
//             return l[1] - r[1];
//         }
//         return l[0] - r[0];
//     });
//     heap.init(jobs);

//     var timeAccum = 0;
//     while(heap.length > 0) {
//         var nextTime = heap.peek()[0];
//         if(time < nextTime) {
//             time = nextTime;
//         }

//         time += heap.peek()[1];
//         var job = heap.pop();

//         timeAccum += time - job[0];
//     }

//     return timeAccum / jobs.length;
// }

function solution(jobs) {
    var waitingJobs = [];
    var leftJobs = jobs.slice();
    leftJobs.sort(function(l, r) {
        return l[0] - r[0];
    });

    var time = 0;
    var timeAccum = 0;
    while(waitingJobs.length > 0 || leftJobs.length > 0) {
        if(waitingJobs.length == 0 && leftJobs[0][0] > time)
        {
            time = leftJobs[0][0];
        }

        while(leftJobs.length > 0 && leftJobs[0][0] <= time) {
            waitingJobs.push(leftJobs.shift());
        }
        waitingJobs.sort((l, r) => l[1] - r[1]);

        var currentJob = waitingJobs.shift();
        time += currentJob[1];
        timeAccum += time - currentJob[0];
    }

    return timeAccum / jobs.length>>0;
}

console.log(solution([[1, 3], [2, 9], [3, 6]]));