// 1. 속한 노래가 많이 재생된 장르부터
// 2. 장르 내에서 많이 재생된 노래부터
// 3. 동일한 재생수일경우 ID값이 낮은거부터
// 4. 장르별 가장 많이 재생된 노래 두개씩만

function solution(genres, plays) {
    var o = {};

    for(var i = 0; i < plays.length; i++) {
        if(!o[genres[i]])
        {
            o[genres[i]] = {};
            o[genres[i]].time = 0;
            o[genres[i]].musics = [];
        }

        o[genres[i]].time += plays[i];
        o[genres[i]].musics.push([i, plays[i]]);
    }

    var sorted_genres = Object.keys(o);
    sorted_genres.sort(function(l,r) {
        return o[r].time - o[l].time;
    });

    var answer = [];
    for(var i = 0; i < sorted_genres.length; i++){
        o[sorted_genres[i]].musics.sort(function(l,r) {
            return r[1] - l[1];
        });
        for (var j = 0; j < 2 && j < o[sorted_genres[i]].musics.length; j++) {
            answer.push(o[sorted_genres[i]].musics[j][0]);
        }
    }

    return answer;
}

console.log(solution(["classic", "pop", "classic", "classic", "pop"],[500, 600, 150, 800, 2500]));