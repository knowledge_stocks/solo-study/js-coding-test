function solution(number, k) {
    var stack = [];

    for(var i = 0; i < number.length; i++) {
        while(k > 0 && stack[stack.length - 1] < number[i]) {
            stack.pop();
            k--;
        }
        stack.push(number[i]);
    }

    if(k > 0) {
        // stack.splice(stack.length - k, k);
        stack = stack.slice(0, stack.length - k);
    }
    return stack.join('');
}

console.log(solution("1924",2,"94"));
console.log(solution("1231234",3,"3234"));
console.log(solution("4177252841",4,"775841"));