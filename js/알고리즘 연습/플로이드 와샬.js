function floydWarshall(n, arr) {
    for(var i = 0; i < n; i++) {
        for(var j = 0; j < n; j++) {
            for(var k = 0; k < n; k++) {
                if(arr[j][k] > arr[j][i] + arr[i][k]) {
                    arr[j][k] = arr[j][i] + arr[i][k];
                }
            }
        }
    }
    return arr;
}

let arr1 = 	[
    [ 0,        Infinity,   41,         10,         24,         25          ],
    [ Infinity, 0,          22,         66,         Infinity,   Infinity    ],
    [ 41,       22,         0,          Infinity,   24,         Infinity    ],
    [ 10,       66,         Infinity,   0,          Infinity,   50          ],
    [ 24,       Infinity,   24,         Infinity,   0,          2           ],
    [ 25,       Infinity,   Infinity,   50,         2,          0           ]
  ];
let arr2 = 	[
    [ 0,        6,          Infinity,   Infinity,   Infinity,   Infinity,   Infinity    ],
    [ 6,        0,          3,          Infinity,   Infinity,   Infinity,   Infinity    ],
    [ Infinity, 3,          0,          Infinity,   Infinity,   1,          Infinity    ],
    [ Infinity, Infinity,   Infinity,   0,          Infinity,   4,          Infinity    ],
    [ Infinity, Infinity,   Infinity,   Infinity,   0,          Infinity,   9           ],
    [ Infinity, Infinity,   1,          4,          Infinity,   0,          Infinity    ],
    [ Infinity, Infinity,   Infinity,   Infinity,   9,          Infinity,   0           ]
  ];
let arr3 = [
    [ 0,        Infinity,   Infinity,   Infinity,   Infinity,   Infinity    ],
    [ Infinity, 0,          Infinity,   8,          12,         6           ],
    [ Infinity, Infinity,   0,          9,          20,         7           ],
    [ Infinity, 8,          9,          0,          Infinity,   7           ],
    [ Infinity, 12,         20,         Infinity,   0,          11          ],
    [ Infinity, 6,          7,          7,          11,         0           ]
  ];

console.log(floydWarshall(arr1.length, arr1));
console.log(floydWarshall(arr2.length, arr2));
console.log(floydWarshall(arr3.length, arr3));